JavAgony
-
Java but with way more agony.

This is a javac plugin to add the retrictions of the JavAgony subset of java.
JavAgony is derived from the [Javagony](https://esolangs.org/wiki/Javagony) java subset and adds even more concerning stuff.
This was a learning project to learn javac plugin development with [Manifold](http://manifold.systems) as helper.


Effects:
-
- No more `if`/`else`/`while`/`do`/`for`/`switch`
- No more methods ( experimental )
- No more casting syntax
- No more `instanceof`
- Member access must use `this` when accessing member of its object ( experimental )
- Constructors must take no parameters ( experimental )


Usage
-
`build.gradle`
```gradle
repositories {
    maven { url 'https://repsy.io/mvn/enderzombi102/plugins' }
    mavenCentral()
}

dependencies {
    annotationProcessor 'com.enderzombi102:javagony:0.1.0'
}
```
