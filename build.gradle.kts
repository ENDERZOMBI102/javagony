plugins {
    `maven-publish`
    java
}

group = "com.enderzombi102"
version = "0.1.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation("systems.manifold:manifold:2022.1.21")
    testAnnotationProcessor( rootProject )
}

tasks.withType<JavaCompile> {
    options.compilerArgs.addAll( listOf(
        "--add-exports", "jdk.compiler/com.sun.tools.javac.api=ALL-UNNAMED",
        "--add-exports", "jdk.compiler/com.sun.tools.javac.code=ALL-UNNAMED",
        "--add-exports", "jdk.compiler/com.sun.tools.javac.tree=ALL-UNNAMED",
        "--add-exports", "jdk.compiler/com.sun.tools.javac.comp=ALL-UNNAMED",
        "--add-exports", "jdk.compiler/com.sun.tools.javac.util=ALL-UNNAMED",
        "--add-exports", "jdk.compiler/com.sun.tools.javac.jvm=ALL-UNNAMED"
    ))
}

tasks.named<JavaCompile>("compileTestJava") {
    options.compilerArgs.add( "-Xplugin:Manifold" )
    options.isIncremental = false
    options.isVerbose = true
    options.isDebug = true
    inputs.property( "rnd", System::nanoTime )

    dependsOn( rootProject.tasks.jar )
    tasks.build.get().dependsOn( this )
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            group = "com.enderzombi102"
            artifactId = "javagony"
            from( components["java"] )
        }
    }

    repositories {
        mavenLocal()
        maven {
            name = "Repsy"
            credentials(PasswordCredentials::class)
            url = uri("https://repsy.io/mvn/enderzombi102/plugins")
        }
    }
}