import java.util.function.Consumer;
import java.util.function.Function;

public class Compiles {
	public Compiles() { }

	public static final Function<String, String> getMessage = name -> "Hello " + name + "!";

	public static final Consumer<String[]> main = argv -> System.out.println( String.class.cast( getMessage.apply("world") ) ); // this must call
}
