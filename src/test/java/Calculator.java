import java.util.function.Consumer;

@Ignored
public class Calculator {
	public static final Consumer<String[]> main = argv -> System.out.println( String.class.cast( Compiles.getMessage.apply("world") ) ); // this must call

	public static final void mian( String[] argv ) {
		main.accept(argv);
	}
}
