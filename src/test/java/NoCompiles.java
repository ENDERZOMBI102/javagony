@Ignored
public class NoCompiles {
	// only in extreme mode
	public NoCompiles(String hello) {
		getMessage( hello );
	}

	public String getMessage(String name) {
		return "Hello " + name + "!";
	};

	public static void main(String[] argv) {
		System.out.println("This is main");

		if ( true ) { } else { }
		for ( var i = 0; i < 2; i++ ) { }
		for ( var i : new Object[] {} ) { }
		switch ( 0 ) {
			case 0: break;
		}
		var x = switch ( 0 ) {
			case 0 -> 0;
			default -> throw new IllegalStateException("Unexpected value: " + 0);
		};
		do { } while ( false );
		while ( true ) { }
	}
}
