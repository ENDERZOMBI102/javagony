package com.enderzombi102.javagony;

import com.sun.source.util.TaskEvent;
import com.sun.source.util.TaskListener;
import com.sun.tools.javac.api.BasicJavacTask;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.List;
import manifold.api.type.ICompilerComponent;
import manifold.internal.javac.TypeProcessor;

import java.util.ArrayList;
import java.util.Objects;

public class JavAgonyProcessor implements ICompilerComponent, TaskListener {
	private BasicJavacTask task;

	@Override
	public void init( BasicJavacTask task, TypeProcessor processor ) {
		this.task = task;
		processor.addTaskListener( this );
	}



	@Override
	public void finished( TaskEvent evt ) {
		if ( evt.getKind() == TaskEvent.Kind.ANALYZE ) {
			var visitor = new JavAgonyVisitor(
				(JCTree.JCCompilationUnit) evt.getCompilationUnit(),
				this.task::getContext,
				evt.getSourceFile()
			);
			for ( var tree : evt.getCompilationUnit().getTypeDecls() ) {
				if ( tree instanceof JCTree.JCClassDecl clazz ) {
					if (
						clazz.getModifiers()
							.getAnnotations()
							.stream()
							.noneMatch( annotation -> annotation.type.tsym.getSimpleName().contentEquals("Ignored") )
					) {
						visitor.visit(clazz);
						// add the additional methods
						for ( var member : visitor.getAdditionalMethodDefs() )
							clazz.sym.members_field.enter( member.sym );
						clazz.defs = List.from( new ArrayList<JCTree>() {{
							addAll( clazz.defs );
							addAll( visitor.getAdditionalMethodDefs() );
						}}.stream().filter( Objects::nonNull ).toList() );
						// debug print
						System.out.println( ( (JCTree.JCClassDecl) tree ).defs );
					}
				}
			}
		}
	}
}