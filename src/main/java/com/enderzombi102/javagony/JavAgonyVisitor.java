package com.enderzombi102.javagony;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.code.Symbol.MethodSymbol;
import com.sun.tools.javac.code.Symbol.VarSymbol;
import com.sun.tools.javac.jvm.Code;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.tree.TreeTranslator;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Names;
import manifold.api.util.JavacDiagnostic;
import manifold.internal.javac.IssueReporter;

import javax.lang.model.element.Modifier;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.util.ArrayList;
import java.util.function.Supplier;

import static com.enderzombi102.javagony.Util.has;

public class JavAgonyVisitor extends TreeTranslator {
	private final IssueReporter<JavaFileObject> reporter;
	private final JavaFileObject file;
	private final TreeMaker maker;
	private final Names names;
	private final Types types;
	private final Symtab syms;
	private JCTree.JCClassDecl clazz;
	private final java.util.List<JCTree.JCMethodDecl> trees = new ArrayList<>();

	public JavAgonyVisitor( JCTree.JCCompilationUnit unit, Supplier<Context> ctxSupplier, JavaFileObject file ) {
		this.reporter = new IssueReporter<>( ctxSupplier );
		this.file = file;
		this.maker = TreeMaker.instance( ctxSupplier.get() ).forToplevel( unit );
		this.names = Names.instance( ctxSupplier.get() );
		this.types = Types.instance( ctxSupplier.get() );
		this.syms = Symtab.instance( ctxSupplier.get() );
	}

	@Override
	public void visitIf( JCTree.JCIf that ) {
		reportError( that.getStartPosition(), "Can't use `if`s!" );
		super.visitIf( that );
	}

	@Override
	public void visitForLoop( JCTree.JCForLoop that ) {
		reportError( that.getStartPosition(), "Can't use `for`s!" );
		super.visitForLoop( that );
	}

	@Override
	public void visitForeachLoop( JCTree.JCEnhancedForLoop that ) {
		reportError( that.getStartPosition(), "Can't use `for`s!" );
		super.visitForeachLoop( that );
	}

	@Override
	public void visitSwitch( JCTree.JCSwitch that ) {
		reportError( that.getStartPosition(), "Can't use `switch`s!" );
		super.visitSwitch( that );
	}

	@Override
	public void visitSwitchExpression( JCTree.JCSwitchExpression that ) {
		reportError( that.getStartPosition(), "Can't use `switch` expressions!" );
		super.visitSwitchExpression( that );
	}

	@Override
	public void visitDoLoop( JCTree.JCDoWhileLoop that ) {
		reportError( that.getStartPosition(), "Can't use `do`s!" );
		super.visitDoLoop( that );
	}

	@Override
	public void visitWhileLoop( JCTree.JCWhileLoop that ) {
		reportError( that.getStartPosition(), "Can't use `while`s!" );
		super.visitWhileLoop( that );
	}

	@Override
	public void visitMethodDef( JCTree.JCMethodDecl that ) {
		// TODO: Redo this to be more clever and less messy
		reporter.reportWarning( that.getName().toString() + ": " + that.getModifiers() );
		if ( has( that.getModifiers().flags, Flags.SYNTHETIC ) ) {
			if ( that.getReturnType() != null ) {
				reportError( that.getStartPosition(), "Can't declare methods in extreme mode!" );
			} else if ( that.getName().contentEquals( "main" ) ) {
				// may be an actual main
				var params = that.getParameters();
				if (
					params.size() != 1 || (
						params.stream().allMatch( param ->
							param.getName().contentEquals( "argv" ) &&
								param.getType().type.hasTag( TypeTag.ARRAY ) &&
								param.getType().type.tsym.getQualifiedName().contentEquals( "java.lang.String" )
						)
					)
				) {
					// not main
					reportError( that.getStartPosition(), "Can't declare methods outside of `main(String[] argv)`!" );
				}
			} else if ( that.getReturnType() == null ) { // constructor?
				if ( that.getParameters().size() > 0 ) {
					reportError( that.getStartPosition(), "Can't declare constructors with more than `0` parameters!" );
				}
			} else
				reportError( that.getStartPosition(), "Can't declare methods outside of `main(String[] argv)`!" );
		}
		super.visitMethodDef( that );
	}

	@Override
	public void visitConditional( JCTree.JCConditional that ) {
		reportError( that.getStartPosition(), "Can't use ternary operator!" );
		super.visitConditional( that );
	}

	@Override
	public void visitTypeCast( JCTree.JCTypeCast that ) {
		if ( !that.toString().contains( ".cast(" ) )
			reportError( that.getStartPosition(), "Can't do casting, use `Result.class.cast(obj)`!" );
		super.visitTypeCast( that );
	}

	@Override
	public void visitTypeTest( JCTree.JCInstanceOf that ) {
		reportError( that.getStartPosition(), "Can't use `instanceof`!" );
		super.visitTypeTest( that );
	}

	@Override
	public void visitVarDef( JCTree.JCVariableDecl that ) {
		if ( that.getName().contentEquals( "main" ) ) {
			// field called main
			if ( that.mods.getFlags().containsAll( List.of( Modifier.STATIC, Modifier.FINAL, Modifier.PUBLIC ) ) ) {
				// its static, final and public
				if ( that.getType().type.tsym.getQualifiedName().contentEquals( "java.util.function.Consumer" ) ) {
					var methodRetType = maker.Type( syms.voidType );
					var methodName = names.fromString( "main" );

					// region GENERATE METHOD PARAMS
					var argvParamId = maker.Ident( names.fromString( "argv" ) );
					var argvParamDecl = maker.VarDef(
						maker.Modifiers( Flags.FINAL, List.nil() ),
						argvParamId.name,
						maker.Type( types.makeArrayType( syms.stringType ) ),
						null
					);
					// endregion

					// region GENERATE METHOD BODY
					// main.accept( argv )
					var funcSelector = maker.Select( maker.Select( maker.Ident( clazz.name ), that.name ), names.fromString( "accept" ) );
					var call = maker.Apply( List.nil(), funcSelector, List.of( argvParamId ) );
					var callSymbol = syms.enterClass( syms.java_base, names.fromString( "java.util.function.Consumer" ) )
						.members()
						.findFirst( names.fromString( "accept" ) );

					call.type = callSymbol.type;
					var statements = List.<JCStatement>of( maker.Exec( call ) );

					var methodBody = maker.Block( Flags.SYNTHETIC, statements );
					// endregion

					// create method decl
					List<JCTypeParameter> methodGenericParams = List.nil();
					List<JCVariableDecl> parameters = List.of( argvParamDecl );
					List<JCExpression> throwsClauses = List.nil();

					var decl = maker.MethodDef(
						maker.Modifiers( Flags.PUBLIC | Flags.SYNTHETIC | Flags.FINAL | Flags.STATIC, List.nil() ),
						methodName,
						methodRetType,
						methodGenericParams,
						parameters,
						throwsClauses,
						methodBody,
						null
					);

					// fix "mirror"
					var methodSymbol = new MethodSymbol(
						decl.getModifiers().flags,
						decl.getName(),
						new Type.MethodType(
							List.of( types.makeArrayType( syms.stringType ) ),
							syms.voidType,
							List.nil(),
							syms.methodClass
						),
						clazz.sym
					);
					{
						var code = methodSymbol.code = new Code( methodSymbol, false, null, false, , false, maker. );
						code.emitInvokeinterface( callSymbol, callSymbol.owner.type );
					}
					decl.sym = methodSymbol;
					decl.type = methodSymbol.type;
					argvParamDecl.sym = new VarSymbol(
						argvParamDecl.getModifiers().flags,
						argvParamDecl.getName(),
						argvParamDecl.vartype.type,
						methodSymbol
					);
					argvParamDecl.type = argvParamDecl.vartype.type;

					// add method to class
					clazz.defs = clazz.defs.append( decl );
					clazz.sym.members().enter( methodSymbol );

					visitMethodDef( decl );
				}
			}
		}
		super.visitVarDef( that );
	}

	@Override
	public void visitIdent( JCTree.JCIdent tree ) {
		super.visitIdent( tree );
	}

	@Override
	public void visitSelect( JCTree.JCFieldAccess that ) {
		// TODO: Implement "must use `this` to access fields"
//		if ( extremeMode ) {
//			reporter.reportInfo( "Sel: " + that.selected.toString() );
//			reporter.reportInfo( "Typ: " + that.type.toString() );
//		}
		super.visitSelect( that );
	}

	@Override
	public void visitApply( JCMethodInvocation tree ) {
		super.visitApply( tree );
	}

	/**
	 * Report an error.
	 * This makes the compilation fail, use with caution.
	 *
	 * @param pos position of the failing block
	 * @param msg message to tell the user what is failing
	 */
	private void reportError( int pos, String msg ) {
		reporter.report(
			new JavacDiagnostic(
				file,
				Diagnostic.Kind.ERROR,
				pos,
				0,
				0,
				msg
			)
		);
	}

	public void visit( JCTree.JCClassDecl clazz ) {
		this.trees.clear();
		( this.clazz = clazz ).accept( this );
	}

	public java.util.List<JCTree.JCMethodDecl> getAdditionalMethodDefs() {
		return this.trees;
	}
}
