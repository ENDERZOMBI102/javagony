package com.enderzombi102.javagony;

public class Util {
	public static boolean has( long flags, int flag ) {
		return ( flags & flag ) == 0;
	}
}
